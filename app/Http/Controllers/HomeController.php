<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Posts;

class HomeController extends Controller
{
    public function index ()
    {
        $message = Posts::where('title', 'title')->first()->body;
        return view('home.index', ['message' => $message]);
    }
}
